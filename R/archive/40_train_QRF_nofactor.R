#------------------------------------------------------------------------------
# Name:     40_model_QRF_nofactor.R
#           (QRF = quantile regression forest, QRFSI = QRF for spatial interpolation)
#
# Content:  - read in BIS regression matrix and select target (response) variable
#           - DO NOT CHANGE CATEGORICAL VARIABLES TO FACTORS
#           - split into training and test data
#           - fit QRF model using training data and "quantregForest" pkg
#           - save target variable data set and model fit model to disk 
#
# Refs:     - QRF package and vignettes:
#             https://cran.r-project.org/web/packages/quantregForest/quantregForest.pdf
#             https://mran.microsoft.com/snapshot/2015-07-15/web/packages/quantregForest/vignettes/quantregForest.pdf
#           - QRFSI:
#             https://github.com/AleksandarSekulic/RFSI
#           
# Inputs:   - regression matrix of entire BIS: out/data/model/tbl_regmat_BIS.Rds
#
# Output:   - saved target variable data set
#           - saved QRF model fit: out/data/model 
#
# Runtime:  - approx. 10 min entire script (much faster because no factors)
#
# Project:  BIS+
# Author:   Anatol Helfenstein
# Updated:  January 2021
#------------------------------------------------------------------------------



### empty memory and workspace; load required packages ----------------------
gc()
rm(list=ls())

# install latest version of R meteo package:
# install.packages("meteo", repos="http://R-Forge.R-project.org")

pkgs <- c("tidyverse", "raster", "meteo", "quantregForest", "viridis", "foreach")
lapply(pkgs, library, character.only = TRUE)



### Prepare modelling data --------------------------------------------------

# locate, read in and stack covariates to predict over
v_cov_names <- dir("out/data/covariates/final_stack",
                   pattern = "\\.grd$", recursive = TRUE)

ls_r_cov <- foreach(cov = 1:length(v_cov_names)) %do%
  raster(paste0("out/data/covariates/final_stack/", v_cov_names[[cov]]))

r_stack_cov <- stack(ls_r_cov)

# read in regression matrix of entire BIS
system.time(
  tbl_regmat_BIS <- read_rds("out/data/model/tbl_regmat_BIS.Rds")
  ) # time elapse: 7 min to read in data!

# possible target soil properties
tbl_regmat_BIS %>% 
  dplyr::select(soil_target) %>% 
  unnest_legacy() %>% 
  colnames()

# select soil property of interest (target/dependent/response variable)
tbl_regmat_target <- tbl_regmat_BIS %>% 
  unnest_legacy(soil_target, .preserve = c(cov, soil_chem, soil_phys, soil_profile,
                                           env_fact, metadata, unknown)) %>% 
  dplyr::select(BIS_tbl:d_mid, pH_KCl, cov) %>% 
  filter(!pH_KCl %in% NA) %>% 
  unnest_legacy(cov)

# split into training and test set (LSK or CCNL = test set)
tbl_regmat_target <- tbl_regmat_target %>% 
  mutate(split = case_when(!BIS_tbl %in% "CCNL" & !BIS_tbl %in% "LSK" ~ "train",
                           BIS_tbl %in% "CCNL" | BIS_tbl %in% "LSK" ~ "test"),
         .before = "BIS_tbl")



### Fit QRF model ------------------------------------------------------------

# QRF parameters:
# - mtry: sets number of variables to try for each split when growing the tree;
#   same default is used as in randomForest (one third of the number of predictors)  
# - nodesize: fixes minimal number of instances in each terminal node, determining
#   how many observations at least lie in the same node (default = 10)
# - ntree: how many trees are grown in RF on which QRF are based on;
#   empirical evidence suggests that performance of prediction remains good even
#   when using only few trees (default = 100 trees)

# save dataset used for modelling to disk
write_rds(tbl_regmat_target, "out/data/model/tbl_regmat_pH_KCl_nofactor.Rds")

# set up parallel backend (requires too much memory to use all cores of SGL Server)
# threads <- parallel::detectCores()
threads <- 10L # try with different amounts of cores

# set mtry to 1/3 of # or predictors
n_mtry <- tbl_regmat_target %>% 
  dplyr::select(-(split:hor), -pH_KCl) %>% 
  ncol()/3 %>% 
  round(., 0) # not sure why round does not work???
n_mtry <- round(n_mtry)

# fit a QRF model
system.time(
  qrf_fit <- quantregForest(x = tbl_regmat_target %>%
                              filter(split %in% "train") %>%
                              dplyr::select(-(split:hor), -pH_KCl),
                            y = tbl_regmat_target %>% 
                              filter(split %in% "train") %>%
                              .$pH_KCl,
                            nthreads = threads,
                            keep.inbag = TRUE,
                            importance = TRUE,
                            quantiles = c(0.05,0.5,0.95),
                            ntree = 500,
                            mtry = n_mtry)
  )
# maybe NAs depends on mtry (60) and ntree (500) with certain amount of cores?
# time elapse (10 cores, 16K obs, 187 var): 10 min

# or read in already previously calibrated model
# qrf_fit <- read_rds("out/data/model/qrf_fit_pH_KCl_obs21k_p181.Rds")

# summary of calibrated model (model fit)
qrf_fit

# Calculated nodes for grown forest; as opposed to RF, where per default values not saved
# obs 1 lies in the nodes in row 1, observation 2 in the nodes in row 2, etc.
qrf_fit$origNodes
# why is this NULL???

# save fitted QRF model
write_rds(qrf_fit, paste0('out/data/model/QRF_fit_', colnames(tbl_regmat_target[13]),
                          '_obs', length(qrf_fit$y), '_p',
                          length(qrf_fit$forest$xlevels), '_nofactor.Rds'))


