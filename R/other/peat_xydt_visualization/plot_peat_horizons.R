## Author:   Luc Steinbuch (SGL)
## see also: https://git.wageningenur.nl/stein012/2022_07_visualising_peat_change


plot_peat_horizons <- function(n_category, 
                               x1 = 1,
                               n_width = 0.1,  # of one plotted profile
                               n_distance = 0.3 # between the shallow and deep profile
)
{
  
  
  ##  Shallow  profile
  if(df_peat_horizons$shallow_start_peat[n_category] > 0)  
  {
    rect(xleft   = x1 - n_distance  - n_width/2, 
         ybottom = df_peat_horizons$shallow_start_peat[n_category], 
         xright  = x1 - n_distance + n_width/2, 
         ytop    = 0, 
         col     = "white")
  }
  
  rect(xleft   = x1 - n_distance  - n_width/2,  
       ybottom = df_peat_horizons$shallow_end_peat[n_category], 
       xright  = x1 - n_distance  + n_width/2,  
       ytop    = df_peat_horizons$shallow_start_peat[n_category], 
       col     = "brown")
  
  
  
  
  if(df_peat_horizons$rest_perhaps_peat[n_category])
  {
    rect(xleft   = x1 - n_distance - n_width/2,  
         ybottom = 130, 
         xright  = x1 - n_distance + n_width/2,  
         ytop    = df_peat_horizons$shallow_end_peat[n_category], 
         col     = "orange"
    )
  } else
  {
    rect(xleft   = x1 - n_distance - n_width/2, 
         ybottom = 130, 
         xright  = x1 - n_distance + n_width/2,  
         ytop    = df_peat_horizons$shallow_end_peat[n_category], 
         col     = "white"
    )  
    
  }
  
  ##  Deep profile
  
  if(df_peat_horizons$deep_start_peat[n_category] > 0)  
  {
    rect(xleft   = x1 +  n_distance  - n_width/2, 
         ybottom = df_peat_horizons$deep_start_peat[n_category], 
         xright  = x1 +  n_distance + n_width/2, 
         ytop    = 0, 
         col     = "white")
  }
  
  rect(xleft   = x1 +  n_distance  - n_width/2,  
       ybottom = df_peat_horizons$deep_end_peat[n_category], 
       xright  = x1 +  n_distance  + n_width/2,  
       ytop    = df_peat_horizons$deep_start_peat[n_category], 
       col     = "brown")
  
  
  
  
  if(df_peat_horizons$rest_perhaps_peat[n_category])
  {
    rect(xleft   = x1 +  n_distance - n_width/2,  
         ybottom = 130, 
         xright  = x1 +  n_distance + n_width/2,  
         ytop    = df_peat_horizons$deep_end_peat[n_category], 
         col     = "orange"
    )
  } else
  {
    rect(xleft   = x1 +  n_distance - n_width/2, 
         ybottom = 130, 
         xright  = x1 +  n_distance + n_width/2,  
         ytop    = df_peat_horizons$deep_end_peat[n_category], 
         col     = "white"
    )  
    
  }
  
  
  ## Connecting lines ####
  
  lines(x = c(x1 - n_distance + n_width/2, x1 +  n_distance - n_width/2), 
        y = c(df_peat_horizons$shallow_start_peat[n_category], df_peat_horizons$deep_start_peat[n_category]) 
  )
  
  lines(x = c(x1 - n_distance + n_width/2, x1 +  n_distance - n_width/2), 
        y = c(df_peat_horizons$shallow_end_peat[n_category], df_peat_horizons$deep_end_peat[n_category]) 
  )
  
  
  ## Combined ####
  
  if( min(df_peat_horizons$shallow_start_peat[n_category], df_peat_horizons$deep_start_peat[n_category]) > 0)
  {
    n_start_no_peat_top <- 0
    n_end_no_peat_top <- min(df_peat_horizons$shallow_start_peat[n_category], df_peat_horizons$deep_start_peat[n_category])
  } else
  {
    n_start_no_peat_top <- 0
    n_end_no_peat_top <- 0
  }
  

  
  if(!df_peat_horizons$rest_perhaps_peat[n_category])
  {
    n_start_no_peat_bottom <- max(df_peat_horizons$shallow_end_peat[n_category], 
                                  df_peat_horizons$deep_end_peat[n_category])
    n_end_no_peat_bottom <- 130
  } else
  {
    n_start_no_peat_bottom <- 0
    n_end_no_peat_bottom <- 0
  }
  
  # background pattern
  
  rect(xleft = x1 +  3*n_distance - n_width/2, 
       ybottom = 130, 
       xright = x1 +  3*n_distance + n_width/2, 
       ytop = 0, 
       col = "orange"#,
       # density = 10,
       # lwd = 3
  )
  
  # rect(xleft = x1 +  3*n_distance - n_width/2, 
  #      ybottom = 130, 
  #      xright = x1 +  3*n_distance + n_width/2, 
  #      ytop = 0, 
  #      col = "brown",
  #      density = 10,
  #      lwd = 3,
  #      angle = 135
  # )
  
  rect(xleft = x1 +  3*n_distance - n_width/2, 
       ybottom = n_end_no_peat_top, 
       xright = x1 +  3*n_distance + n_width/2, 
       ytop = n_start_no_peat_top, 
       col = "white"
  )
  
  rect(xleft = x1 +  3*n_distance - n_width/2, 
       ybottom = n_end_no_peat_bottom, 
       xright = x1 +  3*n_distance + n_width/2, 
       ytop = n_start_no_peat_bottom, 
       col = "white"
  )
  
  
  ## Add labels to x-axis  
  axis( side = 1, 
        at= x1 + c(-n_distance, n_distance, 3*n_distance),
        labels= paste0(n_category, c(" Shallow", " Deep", " Combined")), 
        las=2,
        cex.axis = 0.7
  )
  
  return(
    list( n_start_no_peat_top = n_start_no_peat_top,
          n_end_no_peat_top = n_end_no_peat_top, 
          n_start_no_peat_bottom = n_start_no_peat_bottom, 
          n_end_no_peat_bottom  = n_end_no_peat_bottom)
  )
  
}


