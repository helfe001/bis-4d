### README for Rproject BISplus ###



# Installation of required R packages in Windows WUR laptop

# When installing packages, since we are not using R provided by WUR, have to
# make new default library directory (C:\Users\helfe001\R_libs_anatol)

# I set this globally for my computer in Control Panel\User Accounts\User Accounts

# -> change my environment variables -> new -> name: R_LIBS_USER and set proper path



# Oracle ODBC drivers on Windows WUR laptop

# Verify that odbc recognizes the installed drivers

# On windows computer, open "ODBC Data Source Administrator (64-bit)" and add

# an oracle client that drives connection to BIS database (only have to do this once)

# Details of connection are in tnsnames.ora file (host, port, etc.)