#### Readme for AHN3 (DTM) NO DATA at 25m resolution ####
By A.F. Helfenstein
2021-12-12


#### ORIGINAL ####
Name:		AHN3_5m_DTM_YesNoData.lyr
Source:		\\WUR\dfs-root\ESG\Shares\Arc\LayerFiles\Elevation_Hoogte\AHN3 (2020)\AHN3-5m
Format: 	ESRI Layerfile
Type:		raster
Gridsize:	5m x 5m
Availability:	
Temporal 
coverage:	2020
Spatial 
coverage:	Netherlands
Projection:	RD_new

Content (EN):	Water bodies, buildings (artificial surfaces) and area outside NL (buitenland)

Language:	



#### PROCESSING ####
1. Open ESRI Layerfile in ArcGIS PRO (now there is maybe also a QGIS version available so no need to first open in ArcGIS...)
and export as GeoTIFF at 5 m resolution.

1. Import AHN3 DTM nodata 5m resolution file into QGIS and warp (reproject) raster from 5m to 25m resolution using method
"cubispline" (runs gdalwarp using GDAL in background). Note this is more conservative (less NA pixels) than
exporting at 25m resolution in ArcGIS, which I think may have used nearest neighbor method...



#### FINAL FILE ####
Name:		ahn3_dtm_nodata_25m.tif
Location:	W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus\data\other
Format: 	GeoTiff files (.tif)
Type:		raster
Gridsize:	25m x 25m
Temporal 
coverage:	
Spatial 
coverage:	National
Projection:	RD_new

Content (EN):	Water bodies, buildings (artificial surfaces) and area outside NL (buitenland)

Language:	