#### Readme for date of updated areas in national soil map 2021 version (bodemkaart 1:50'000) ####
By A.F. Helfenstein & Fokke de Brouwer
2022-05-23


#### SOURCE FILE ####
Name:			National soil map of NL 1:50'000 (updated 2021 version)
Source:			https://bodemdata.nl/basiskaarten (obtained directly from Fokke de Brouwer)
Format: 		
Type:			Vector/Polygons
Availability:		
Temporal
Coverage:		Composed of Kaartbladen made btw. 1961-1995 & updated regions (2014-2021)
Spatial 
coverage:		National / Netherlands
Projection:		RD_new

Content:		National soil map with hundreds of different soil types (detailed legend)

Language:		Dutch
Terms-of use:	



#### PROCESSING ####
1. Make a new column in the attribute table by changing date of updated area column to year (don't need day or month)...

2. Rasterize (GDAL function) in QGIS -> vector to raster using AHN2 as reference raster at 25m resolution.
Select kaartblad year as variable of interest.

3. Change name and save/export raster



#### FINAL FILE ####
Name:			bodem50_2021_update_25m.tif
Directory:		W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus\data\other
Format: 		GeoTIFF
Type:			Raster
Gridsize:		25m x 25m
Temporal
Coverage:		Years at which national soil map was updated (2014-2021)
Spatial 
coverage:		National / Netherlands
Projection:		RD_new

Content:		Year in which each part of NL soil map was updated. Notice that most regions were not updated.

Language:		Dutch