#### Readme for aggregation/classification of different peat horizons ####
By A.F. Helfenstein & Fokke de Brouwer
2022-04-12


#### SOURCE FILE ####
Name:			National soil map of NL 1:50'000 (updated 2021 version)
Source:			https://bodemdata.nl/basiskaarten (obtained directly from Fokke de Brouwer)
Format: 		
Type:			Vector/Polygons
Availability:		
Temporal
Coverage:		Composed of Kaartbladen made btw. 1960-1995 & specific regions that were updated (2014-2021)
Spatial 
coverage:		National / Netherlands
Projection:		RD_new

Content:		National soil map with hundreds of different soil types (detailed legend)

Language:		Dutch
Terms-of use:	



#### PROCESSING (by Fokke de Brouwer) ####
1. Reclassified/aggregated soil types into the following 9 categories/classes based on starting depth & thickness of
horizons containing peat (see content below)

2. Rasterize (GDAL function) in QGIS -> vector to raster using AHN2 as reference raster at 25m resolution

3. Change name and save/export raster



#### FINAL FILE ####
Name:			bodem50_2021_peat_25m.tif
Location:		W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus\data\covariates\soil
Format: 		GeoTIFF
Type:			Raster
Gridsize:		25m x 25m
Temporal
Coverage:		Composed of Kaartbladen made btw. 1960-1995 & specific regions that were updated (2014-2021)
Spatial 
coverage:		National / Netherlands
Projection:		RD_new

Content:		Reclassified/aggregated soil types into the following 9 categories/classes based on starting depth
			& thickness of horizons containing peat:
				0	no determination possible
				1	starting within 0-15 cm below surface and thicker than 40 cm
				2	starting within 0-15 cm below surface and between 15-40 cm  thick
				3	starting within 15-40 cm below surface and thicker than 40 cm
				4	starting within 15-40 cm below surface and between 15-40 cm  thick
				5	starting within 40-80 cm below surface and thicker than 40 cm
				6	starting within 40-80 cm below surface and between 15-40 cm  thick
				7	starting within 80-120 cm below surface and thicker than 40 cm
				8	no peat (thicker than 15 cm) within 120 cm below surface
			This includes so-called "Veengronden" (legend symbol: V, e.g. peat soils), "Moerige gronden"
			(legend symbol: W, e.g. peaty soils), but also other soil types with legend symbols Rv, Mv
			(starting within 0-80cm and thicker than 40cm), ...v (starting within 80-120cm and thicker than 40cm),
			...w (starting within 80-120cm and btw. 15-40cm thickness) and other more locally specialized soil codes

Language:		Dutch