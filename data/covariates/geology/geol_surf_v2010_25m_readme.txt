#### Readme for geological surface map of the Netherlands (2010 version) ####
By A.F. Helfenstein
2021-11-30


#### SOURCE FILE ####
Name:			NL_SurfGeol_v2010.lyr
			NL_SurfGeol_v2010.qlr
Source:			GeoDesk (\\WUR\dfs-root\ESG\Shares\Arc\LayerFiles\Soil_Bodem\Geologie)
Format: 		ESRI / QGIS format LayerFile
Type:			Polygons
Availability:
Temporal
coverage:		(2010 version)
Spatial 
coverage:		National / Netherlands
Projection:		RD_new

Content:		geological units / classes / categories of the Netherlands

Content (detailed):	Deze kaart geeft de afzettingen aan of nabij het maaiveld (of de zeebodem) weer,
			tot een diepte van ca. 5 m. De kaart is samengesteld op grond van bestaande kaarten
			die door de voormalige Rijks Geologische Dienst zijn vervaardigd, waar nodig aangevuld
			met niet eerder gepubliceerde gegevens. De afzettingen zijn ingedeeld volgens de
			nieuwe lithostratigrafische indeling die vanaf 1997 door TNO is ontwikkeld. In deze
			indeling worden de afzettingen onderscheiden op grond van hun lithologische en/of
			seismische eigenschappen, stratigrafische positie en herkomst. Ouderdom van de
			afzettingen speelt bij de classificatie geen rol.

			De onderscheiden eenheden worden beknopt beschreven in het boek van ‘De ondergrond
			van Nederland’ (De Mulder et al., 2003). Uitgebreide beschrijvingen van de eenheden
			zijn te vinden in de Lithostratigrafische nomenclator van de ondiepe ondergrond:
			http://dinoloket.tno.nl. De kaart is uitsluitend geschikt voor gebruik op een schaal
			van 1:600.000 of kleiner.

Language:		Dutch
Terms-of use:	



#### PROCESSING ####
1. Polygon to raster (target attribute = "KRTCODE_NE") using existing DEM (AHN2) to snap the raster to (25m x 25m resolution)
* snap raster = all cells are designated to same geographical location

4. Raster to other format (.tif)

5. Change name from "NL_SurfGeol_v2010_Polygon" to "geol_surf_v2010_25m"



#### FINAL FILE ####
Name:			geol_surf_v2010_25m.tif
Location:		W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\project\BISplus\data\covariates\geology
Format: 		GeoTIFF
Type:			Raster
Gridsize:		25m x 25m
Temporal
coverage:		(2010 version)
Spatial 
coverage:		National / Netherlands
Projection:		RD_new

Content:		geological units / classes / categories of the Netherlands

Content (detailed):	Deze kaart geeft de afzettingen aan of nabij het maaiveld (of de zeebodem) weer,
			tot een diepte van ca. 5 m. De kaart is samengesteld op grond van bestaande kaarten
			die door de voormalige Rijks Geologische Dienst zijn vervaardigd, waar nodig aangevuld
			met niet eerder gepubliceerde gegevens. De afzettingen zijn ingedeeld volgens de
			nieuwe lithostratigrafische indeling die vanaf 1997 door TNO is ontwikkeld. In deze
			indeling worden de afzettingen onderscheiden op grond van hun lithologische en/of
			seismische eigenschappen, stratigrafische positie en herkomst. Ouderdom van de
			afzettingen speelt bij de classificatie geen rol.

			De onderscheiden eenheden worden beknopt beschreven in het boek van ‘De ondergrond
			van Nederland’ (De Mulder et al., 2003). Uitgebreide beschrijvingen van de eenheden
			zijn te vinden in de Lithostratigrafische nomenclator van de ondiepe ondergrond:
			http://dinoloket.tno.nl. De kaart is uitsluitend geschikt voor gebruik op een schaal
			van 1:600.000 of kleiner.

Language:		Dutch