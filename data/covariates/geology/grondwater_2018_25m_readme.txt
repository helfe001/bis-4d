#### Readme for groundwater classes (grondwatertrappe) 2018 version ####
By A.F. Helfenstein
2021-10-21


#### SOURCE FILE ####
Name:			gt_classes2018_NL.lyr
Source:			GeoDesk (\\WUR\dfs-root\ESG\Shares\Arc\LayerFiles\Soil_Bodem\Gwt_50_2018)
Format: 		ESRI format LayerFile
Type:			Raster (50 m x 50 m)
Availability:
Temporal
coverage:		
Spatial 
coverage:		National
Projection:		RD_new
Content (NE/EN):	Groundwater classes (grondwatertrappe):
				0: other/nodata
				1: Ia
				2: Ic
				3: IIa
				4: IIb
				5: IIc
				6: IIIa
				7: IIIb
				8: IVc
				9: IVu
				10: Vad
				11: Vao
				12: Vbd
				13: Vbo
				14: VId
				15: VIId
				16: VIIId
				17: VIIIo
				18: VIIo
				19: VIo

Language:		English/Dutch
Terms-of use:	



#### PROCESSING ####
1. Save as 25m x 25m raster; rename "grondwater_2018_25m.tif"

2. Redefine nodata values (Groundwater only mapped for agricultural land, so all forest and urban areas are NA)
to values 20 (forest, urban areas, and water) using GDAL commands (e.g. on Ubuntu terminal/command line):

gdalwarp -dstnodata 20 grondwater_2018_25m.tif grondwater_2018_25m_nodata20.tif
gdal_edit.py -a_nodata 0 grondwater_2018_25m_nodata20.tif

"gdalwarp" rewrites all nodata cells to value 20, and changed the nodata flag to 20.
"gdal_edit.py" changes the nodata flag back to 0, leaving the pixels untouched. The 20 pixels are now valid.
Rename and overwrite "grondwater_2018_25m_nodata20.tif" back to "grondwater_2018_25m.tif"
(later in BISplus workflow urban areas and water will be masked out)



#### FINAL FILE ####
Name:			grondwater_2018_25m.tif
Location:		W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus_GIS\Preprocessing
			W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus\data\covariates\geology
Format: 		GeoTIFF
Type:			Raster
Gridsize:		25m x 25m
Temporal
coverage:		
Spatial 
coverage:		National
Projection:		RD_new

Content (NE/EN):	Groundwater classes (grondwatertrappe):
				0: other/nodata
				1: Ia
				2: Ic
				3: IIa
				4: IIb
				5: IIc
				6: IIIa
				7: IIIb
				8: IVc
				9: IVu
				10: Vad
				11: Vao
				12: Vbd
				13: Vbo
				14: VId
				15: VIId
				16: VIIId
				17: VIIIo
				18: VIIo
				19: VIo


Language:		Dutch