#### Readme for Sentinel 2 (S2) monthly mosaics of NDVI ####
By A.F. Helfenstein
2021-10-21


#### ORIGINAL ####
Name:		ndvi[year]_[month].dat
Source:		Groenmonitor group WENR (https://www.groenmonitor.nl/)
Format: 	.dat file
Type:		raster (10m x 10m)
Availability:	upon reasonable request
Temporal 
coverage:	monthly mosaics from time period btw. 2016-2021
Spatial 
coverage:	National / Netherlands
Projection:	RD_new

Content:	NDVI (normalized difference vegetation index)

Language:	



#### PROCESSING ####
1. Reproject (e.g. QGIS/GDAL raster tool "warp") from EPSG:6326 to Amersfoort/ RD New (EPSG:28992)
using Cublic-spline method

2. Export as raster, changing resolution from 10m to 25m



#### FINAL FILE ####
Name:		s2_ndvi[year]_[month]_25m.tif
Location:	W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus\data\covariates\organism
		W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus_GIS\Preprocessing
Format: 	GeoTiff (.tif)
Type:		raster
Gridsize:	25m x 25m
Temporal 
coverage:	monthly mosaics from time period btw. 2016-2021
Spatial 
coverage:	National / Netherlands
Projection:	RD_new

Content:	NDVI (normalized difference vegetation index)

Language:	