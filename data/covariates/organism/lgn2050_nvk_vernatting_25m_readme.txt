#### Readme for rewetting areas in nature inclusive land use scenario for 2050 (Natuurverkenning (NVK) 2050) ####
By A.F. Helfenstein
2023-11-19


#### ORIGINAL ####
Name:			bdb2050_t11xlsx_20210712-150611_firstrule.tif
Source:			W:\PROJECTS\Nvk_bdb\a_ARCHIEF\a_geodata\c_2050_v2\c_basiskaart\firstrule
Format: 		GeoTIFF
Type:			raster
Gridsize:		2.5m x 2.5m
Availability:		Closed data; permission required from Hans Roelofsen (WUR, WENR)
Temporal 
coverage:		2050 (scenario)
Spatial 
coverage:		National (Netherlands)
Projection:		RD_new

Content (EN):	binary variable of where rewetting ("vernatting") will take place under nature-inclusive scenarios for 2050
		(see "description" column in lgn2050_nvk_vernatting_25m_reclassify.xlsx)



Language:	Dutch



#### PROCESSING ####
1. Open GeoTIFF file in QGIS
2. Export Raster attribute table using RasterAttributeTable plugin to Excel file
3. Resample to 25m resolution using nearest neighboor method (gdalwarp in QGIS)
4. Export/save GeoTIFF at 25m resolution to working directory



#### FINAL FILE ####
Name:			lgn2050_nvk_vernatting_25m.tif
Location:		W:\ESG\DOW_SGL\Research_PhD\AnatolHelfenstein\BISplus\data\covariates\organism\
Format: 		GeoTiff (.tif)
Type:			raster
Gridsize:		25m x 25m
Temporal 
coverage:		2050
Spatial 
coverage:		National (Netherlands)
Projection:		RD_new

Content (EN):	binary variable of where rewetting ("vernatting") will take place under nature-inclusive scenarios for 2050
		(see "description" column in lgn2050_nvk_vernatting_25m_reclassify.xlsx)


Language:	English