# BIS-4D <img src="img/NL_pH_BIS_avatar.png" align="right" width="250"/>

This is the code repository of the scientific manuscript entitled "BIS-4D: Mapping soil properties and their uncertainties at 25 m resolution in the Netherlands" [Helfenstein et al., 2024a](https://doi.org/10.5194/essd-2024-26). In that manuscript, please see the section "Data and code availability" for more information. The BIS-4D soil property prediction maps at 25 m resolution can be downloaded at <https://doi.org/10.4121/0c934ac6-2e95-4422-8360-d3a802766c71>. A previous version of the code was provided with [BIS-3D](https://github.com/anatol-helfenstein/BIS-3D), which is the code repository of [Helfenstein et al., 2022](https://doi.org/10.1016/j.geoderma.2021.115659). In addition, there is a [R tutorial available for modelling soil organic matter (SOM) in 3D space and time](https://git.wageningenur.nl/helfe001/bis-4d_masterclass) using the BIS-4D approach, which is based on [Helfenstein et al., 2024b](https://doi.org/10.1038/s43247-024-01293-y). Model inputs and outputs of BIS-4D use the coordinate reference system most commonly used in the Netherlands: [EPSG:28992 Amersfoort/RD New](https://epsg.io/28992).

## Model inputs

### Soil point data

The soil point data used in BIS-4D, excluding LSK and CCNL data, can be downloaded here: <https://doi.org/10.4121/c90215b3-bdc6-4633-b721-4c4a0259d6dc>. The "bodemkunig informatie systeem" (BIS), the soil database of the Netherlands, contains 3 different types of soil data:

-   BPK = "boring" / boreholes to a max of 5 m depth (most go to 1.2 to 2 m depth); the locations are specifically selected (purposive sampling); mostly field observations and little to no laboratory analysis (?)
-   PFB = "profielbeschrijving" / soil profile descriptions from soil pit the locations are specifically selected (purposive sampling); Almost always, samples were taken for lab analysis
-   LSK = "Landelijke Steekproef Kaarteenheden"; dataset with profile descriptions and samples including lab analysis; stratified random sampling design based on soil type and grondwatertrappe (groundwater), see [Finke et al. 2001](https://library.wur.nl/WebQuery/wurpubs/316229) and [Visschers et al. 2007](https://doi.org/10.1016/j.geoderma.2007.01.008).

In addition there is the "carbon content Netherlands" (CC-NL) dataset, for which LSK sampling sites were revisited 20 years later (2018). Samples were taken for the topsoil (0-30) and for the subsoil (30-100) regardless of location or soil type. Contains a lot of wet chemistry, conventional laboratory analysis as well as spectroscopy measurements (see [Van Tol-Leenders et al. 2019](https://doi.org/10.18174/509781)).

### Covariates

The covariates used in BIS-4D can be downloaded here: <https://doi.org/10.4121/6af610ed-9006-4ac5-b399-4795c2ac01ec>. The covariates were specifically chosen to represent the CLORPT framework, wherein soil is a function of the soil forming factors climate (CL), organisms (O), relief/topography (R), parent material/geology (P) and time (T; [Dokuchaev, 1899](#references); [Jenny, 1941](#references)). For a descriptive summary of the covariates used in this study, see [covariates_metadata.csv](data/covariates/covariates_metadata.csv).

## Model workflow (R scripts)

### 1. Soil data preparation

-   [10_db_connect_ODBC.R](10_db_connect_ODBC.R) - Connect to BIS Oracle database (DB) using oracle client basic and odbc driver; example of how to query DB using dbplyr syntax (***Note:*** for model inputs accessibility, see [data accessibility](#data-accessibility))
-   [11_soil_CCNL_prep.R](11_soil_CCNL_prep.R) - Compile relevant soil property data and geographic locations of sampling locations of the CCNL dataset and export table of soil property data and 3D coordinates of the CCNL dataset
-   [11_soil_PFB_BPK_LSK_prep.R](11_soil_PFB_BPK_LSK_prep.R) - Connect to BIS Oracle database (DB) using oracle client basic and odbc driver, query DB using dplyr syntax, compile relevant soil property data and geographic locations of sampling locations and export table of soil property data and 3D coordinates
-   [12_soil_BIS_master_tibble.R](12_soil_BIS_master_tibble.R) - Combine all BIS soil information: BPK, CC-NL, LSK and PFB lab and field tibbles into one master tibble
-   [15_soil_BIS_expl_analysis_BPK.Rmd](15_soil_BIS_expl_analysis_BPK.Rmd) - Exploratory analysis of BPK dataset
-   [15_soil_BIS_expl_analysis_metadata.Rmd](15_soil_BIS_expl_analysis_metadata.Rmd) - Exploratory analysis of metadata of BIS datasets (e.g. projects, time, sample age, etc.)
-   [15_soil_BIS_expl_analysis_target_BD.Rmd](15_soil_BIS_expl_analysis_target_BD.Rmd) - Exploratory analysis of bulk density (BD) soil point data in BIS
-   15_soil_BIS_expl_analysis_target\_[...].Rmd - Exploratory analysis of potential target soil properties (dependent variables / response) for modelling in BIS-4D. As in the script of BD above but for additional soil properties: cation exchange capacity (CEC), clay content, carbon to nitrogen ratio (CN ratio), nitrogen (N), phosphorus (P), pH, sand content, silt content, loam content, soil organic matter (SOM), soil organic carbon (SOC) and soil carbon (SC).
-   [16_soil_BIS_master_tibble_final.R](16_soil_BIS_master_tibble_final.R) - Based on findings during exploratory analysis of each potential target soil property, make final changes to table of all soil point data in BIS (e.g. remove outliers, change units, remove outdated or less used measurement methods, etc.) and tidy data into one "master" tibble with all soil property data in BIS.

### 2. Covariate preparation

-   [20_cov_prep_gdal.R](20_cov_prep_gdal.R) - Assemble & prepare predictors (covariates as raster data):
    -   Designate coordinate system (projection)
    -   Resample covariates so they have the same origin, cell locations and extent
    -   Mask nodata areas (water and areas outside NL) of continuous covariates (categorical covariates masked after reclassification in script [22_cov_cat_recl_gdal_par.R](22_cov_cat_recl_gdal_par.R))
    -   Assemble into raster stack and save
-   [21_cov_dem_deriv_saga.R](21_cov_dem_deriv_saga.R) - Compute digital elevation model (DEM) derivatives using AHH2 (Dutch DEM, version 2) based on [Hengl & MacMillan, 2019](https://soilmapper.org/)
-   [22_cov_sensing_deriv.R](22_cov_sensing_deriv.R) - Define spectral indice functions according to [Loiseau et al. 2019](#references). Apply functions to remote sensing covariates in BIS+, using R, G, B & NIR bands. Compute PCA of all raw and processed (spectral indices) remote sensing data.
-   [23_cov_cat_recl_gdal.R](23_cov_cat_recl_gdal.R) - Prepare categorical covariates:
    -   Define categorical variables in covariate stack as such
    -   Reclassify: combine levels of each categorical covariate into new levels that are more broad
    -   Designate NA values and mask categorical covariates
    -   Write all covariates to disk (final stack for modelling)
-   [24_cov_dyn_prep_gdal.R](24_cov_dyn_prep_gdal.R) - Define dynamic categorical variables in covariate stack as such. Reclassify: combine levels of each categorical dynamic covariate into new levels that are more broad. Designate NA values and mask dynamic categorical covariates.
-   [25_cov_expl_analysis_clorpt.Rmd](25_cov_expl_analysis_clorpt.Rmd) - Exploratory analysis of covariates, inspecting a few of each CLORPT soil forming factor
-   [25_cov_expl_analysis_cont_cat.Rmd](25_cov_expl_analysis_cont_cat.Rmd) - Exploratory analysis of covariates: histogram, map and, if categorical, pie chart of classes of each covariate, grouped into continuous and categorical covariates
-   [25_SOM_4D_field_campaign_pres.Rmd](25_SOM_4D_field_campaign_pres.Rmd) - Presentation of field campaign to collect soil samples at previous PFB locations from the past for SOM 3D+T modelling.
-   [25_SOM_4D_field_campaign.Rmd](25_SOM_4D_field_campaign.Rmd) - Tutorial and script to select locations for re-visiting previous PFB locations from the past for SOM 3D+T modelling.

### 3. Overlay soil point data with covariates and regression matrix

-   [30_regression_matrix_BIS.R](30_regression_matrix_BIS.R) - Read in prepared covariate stack and soil point data with coordinates; overlay rasters and points and prepare regression matrix (extract covariate values from sampled/observed locations)
-   [31_regression_matrix_RFE.R](31_regression_matrix_RFE.R) - Select TARGET variable (response), removing all rows with no observations for TARGET soil property. If calibration data includes field estimates, remove field estimates from locations and horizons for which lab measurements are present. RFE step 1: Remove covariates whose pearson correlation \> 0.85. Prepare regression matrix for model calibration for iterative RFE: turn categorical covariates into factors, split into training and test data. RFE step 2: RFE using caret::rfe() with OUTER and INNER k-fold CV using default RF ranger hyperparameters.
-   [32_regression_matrix_dyn_4D.R](32_regression_matrix_dyn_4D.R) - Overlay rasters and points and prepare regression matrix (extract covariate values from sampled locations), derive covariates dynamic over 2D space & time (2D+T; "[]*xyt*[]") and 3D+T ("[]*xydt*[]") based on land use (LU) maps and maps of peat horizon occurrences. Change re-visited PFB sites from field campaign into validation locations.
-   [35_model_data_expl_analysis_pH_KCl.Rmd](35_model_data_expl_analysis_pH_KCl.Rmd) - Exploratory analysis of all input data related to target soil property pH used for model calibration and validation.
-   [35_model_data_expl_analysis_SOM.Rmd](35_model_data_expl_analysis_SOM.Rmd) - Exploratory analysis of all input data related to target soil property SOM used for model calibration and validation.
-   [35_model_data_expl_analysis.Rmd](35_model_data_expl_analysis.Rmd) - Exploratory analysis of all input data related to all target soil properties (not only pH and SOM) used for model calibration and validation.

### 4. Model calibration (training)

-   [40_train_RF_hyperparameter_tuning.R](40_train_RF_hyperparameter_tuning.R) - Tune random forest (RF) hyperparameters:
    -   Read in BIS regression matrix and select target (response) variable
    -   Turn categorical variables into factors
    -   Split into training and test data
    -   Fit RF models all using a cross-validation (CV) grouped by location of the training data (requires "caret", "CAST" and "ranger" pkg ("ranger" is preferable because much faster)
    -   Different RF models are fit using a full cartesian grid of hyperparameters (e.g. ntree, mtry, node size, splitrule, resampling type and size) and model performance values for each set of hyperparameters is saved
-   [41_train_QRF_optimal_model.R](41_train_QRF_optimal_model.R) - Calibrate/train QRF with optimal hyperparameters:
    -   Read in regression matrix and tuning grid of hyperparameters specific to chosen target soil property (response/dependent var)
    -   Fit QRF model using a cross-validation (CV) grouped by location of the training data (requires "caret", "CAST" and "quantregForest" or "ranger" pkg ("ranger" is preferable because much faster)) and optimal hyperparameters
    -   Save trained/fitted/calibrated model to disk

### 5. Model evaluation (validation/test)

Model were evaluated using accuracy plots (i.e. predicted vs. observed)) and the following metrics: mean error (ME), mean squared error (MSE), root mean squared error (RMSE), model efficiency coefficient (MEC) and the prediction interval coverage probability (PICP) of PI90. Different external accuracy assessment (statistical validation) strategies were used: cross-validation of PFB dataset and design-based inference of the LSK and/or CCNL probability sample separated by depth layer.

-   [50_preparation_10F-CV_folds1-5.R](50_preparation_10F-CV_folds1-5.R) - Use "ranger" pkg to predict mean and all quantiles of first 5 folds from K-fold CV (usually K = 10). Note: due to RAM limit, not all folds could be read into Rsession and predictions made at once (remaining folds and entire model evaluation is done in next script [51_model_evaluation_all_depths.R](51_model_evaluation_all_depths.R))
-   [51_model_evaluation_all_depths.R](51_model_evaluation_all_depths.R) - Evaluate map accuracy using 3 statistical validation strategies. Make predicted vs. observed plots and accuracy metrics for all statistical validation strategies over all depths combined. Include Prediction Interval Coverage Probability (PICP) of 90% prediction intervals (PI90) in plots. Calculate PICP of all PIs for all statistical validation strategies.
-   [52_model_evaluation_all_depths_soil_texture.R](52_model_evaluation_all_depths_soil_texture.R) - Same as script [51_model_evaluation_all_depths.R](51_model_evaluation_all_depths.R), but here we post-process soil texture (clay, silt and sand content) predictions so that they add up to 100%.
-   [53_model_evaluation_depth_layers.R](53_model_evaluation_depth_layers.R) - Evaluate map accuracy using different statistical validation strategies but separated into GSM depth layers. This allows for design-based statistical inference using LSK and CCNL probability samples.
-   [53_model_evaluation_depth_layers_agri.R](53_model_evaluation_depth_layers_agri.R) - Same as above, but only for agricultural areas in the Netherlands.
-   [53_model_evaluation_depth_layers_CCNL.R](53_model_evaluation_depth_layers_CCNL.R) - Same as in [53_model_evaluation_depth_layers.R](53_model_evaluation_depth_layers.R), but separated into depth layers sampled during CCNL (0-30 cm, 30-100 cm), as well as 100-200 cm.
-   [54_model_evaluation_dyn_PFBval_2022.R](54_model_evaluation_dyn_PFBval_2022.R) - Statistical validation of BIS-4D (3D+T) model predictions using PFB validation data, i.e. re-visited PFB sites of BIS in 2022. Compute accuracy metrics and plots of difference in TARGET model predictions over time (delta TARGET).
-   [55_model_evaluation_var_imp_xML.R](55_model_evaluation_var_imp_xML.R) - Help explain/interpret ML/AI (xML/xAI) using 3 methods: 1) Variable importance (permutation or impurity), 2) Partial dependence plots (PDP): TARGET/response variable vs. predictors/covariates, 3) Local xML method: Shapley values for time series predictions (only reached testing phase)
-   [56_model_evaluation_all_soil_properties.R](56_model_evaluation_all_soil_properties.R) - Combine accuracy metrics from models of all BIS-4D target soil properties and create tables for each soil property separated by metric or by depth layer.

### 6. Soil prediction maps

-   [60_soil_maps_predict_QRF.R](60_soil_maps_predict_QRF.R) - Compute soil maps:
    -   Define target variable (response) and target prediction depth
    -   Read in fitted quantile regression forest (QRF) model
    -   Read in stack of covariates (predictors)
    -   Predict response mean and quantiles (e.g. 50<sup>th</sup>/median, 5<sup>th</sup> & 95<sup>th</sup> quantiles) using terra::predict and ranger::predict arguments
    -   Calculate PI90 & GSM accuracy thresholds
    -   Visualize maps and write rasters of results to disk
-   [61_soil_maps_predict_QRF_transect.R](61_soil_maps_predict_QRF_transect.R) - Predict target soil property (response) mean and quantiles (e.g. 50th/median, 5th & 95th quantiles) across designated depth transects.
-   [62_soil_maps_soil_texture.R](62_soil_maps_soil_texture.R) - Post-process soil texture / particle size fraction (PSF) response variables (BIS-4D target soil properties) so that they add up to 100%.
-   [63_soil_maps_visualize_dyn.R](63_soil_maps_visualize_dyn.R) - Create visualizations and plot maps of mean and median predictions of first (1953) & last (2023) prediction year at each standard GSM depth. Plot maps of differences (delta TARGET) in mean and median predictions, respectively, between last and first year. Plot maps of PI90 of first (1953) & last (2023) prediction year at each standard GSM depth. Export images (png) of each year of each standard GSM depth layer & create GIF animation of predictions at one GSM depth layer over all years.
-   [63_soil_maps_visualize_static.R](63_soil_maps_visualize_static.R) - Create visualizations and plot maps of mean, median, 5th and 95th quantile predictions and the PI90 for static target soil properties.
-   [64_soil_maps_spatial_avg_dyn.R](64_soil_maps_spatial_avg_dyn.R) - Compute average changes for groups of land use or soil type based predictions of dynamic soil properties for different years.

### 7. Time series predictions at point locations

-   [70_predict_dyn_time_series.R](70_predict_dyn_time_series.R) - Create time series plots of predictions at point locations across time. Compare with validation data from years for which these are available (2 points in time).

## Summary of supporting scripts, files and directories

-   [R/other](R/other/) - R scripts that are not part of core modelling workflow, i.e. do not need to be re-run when adding new data or changing model parameters. Some examples of script names and their description are provided below (incomplete, based on older version):
    -   [QRF_comparison_approaches.R](R/other/QRF_comparison_approaches.R) - Benchmark and test different QRF training and predict functions from different packages
    -   [color_schemes.R](R/other/color_schemes.R) - Explore different color schemes for continuous and categorical covariates and soil maps
    -   [compare_AHN.R](R/other/compare_AHN.R) - Compare different AHN (digital elevation model (DEM) of the Netherlands) versions by subtracting rasters
    -   [comparison_SoilGrids_evaluation_depth_layers_LSK-SRS.R](R/other/comparison_SoilGrids_evaluation_depth_layers_LSK-SRS.R) - Map accuracy of SoilGrids for NL using LSK-SRS probability sample as a comparison to BIS-3D maps.
    -   [extract_from_raster_comparison.R](R/other/extract_from_raster_comparison.R) - Benchmark and compare different extracting methods (i.e. extracting covariate data at soil observation locations)
    -   [make_recl_tbl.R](R/other/make_recl_tbl.R) - Make reclassifying metadata table for each categorical covariate based on ID values in raster S4 attribute table. This script was only run once to create a unique template for each categorical covariate reclassification table **prior** to manually describing and aggregating each class ID. ***CAUTION: Running this script will overwrite all the classes of each categorical covariate that were manually inserted based on expert knowledge!***
    -   [merge_dbl_cols.R](R/other/merge_dbl_cols.R) - Tests how to merge double columns in soil point data tabular data in a sensible way
    -   [predict_QRF_soil_maps_tiles.R](R/other/predict_QRF_soil_maps_tiles.R) - Predict soil property for each tile of tiled raster stack and then merge back together to get one map of all of the Netherlands (***Note:*** tiling may be useful for users with RAM issues)
    -   [predict_qrf_fun.R](R/other/predict_qrf_fun.R) - Modify tweaked version of ranger function so that mean can also be computed directly from QRF (slightly altered from [ISRIC's tweaked QRF predict function](https://git.wur.nl/isric/soilgrids/soilgrids/-/blob/master/models/ranger/predict_qrf_fun.R))
    -   [target_prediction_depth_GSM.R](R/other/target_prediction_depth_GSM.R) - Create target *GSM* prediction depth layers
    -   [tiling_rasters.R](R/other/tiling_rasters.R) - Functions (sequential and parallel) to tile rasters (***Note:*** tiling may be useful for users with RAM issues; see also [split raster into tiles](https://stackoverflow.com/questions/52484216/split-a-raster-into-5-pixel-x-5-pixel-tiles-in-r))
-   [data](data/) - Input data for modelling workflow (***Note:*** for model inputs accessibility, see [data accessibility](#data-accessibility))
    -   [covariates](data/covariates/) - metadata (e.g. README and reclassification table files) of covariates, which are based on the soil forming factors climate, geology, organism and relief (***Note:*** The soil forming factor time is incorporated indirectly in the other soil forming factors since the covariates represent data from different times).
        -   [climate](data/covariates/climate/) - README files ([...]\_readme.txt) of covariates related to soil forming factor climate
        -   [geology](data/covariates/geology/) - README, reclassification table ([...]\_reclassify.csv and [...]\_reclassify.xlsx) and attribute table of original classes ([...]\_attributes.csv) files of covariates related to soil forming factor geology/parent material
        -   [organism](data/covariates/organism/) - README, reclassification table, attribute table of original classes and any other metadata files of covariates related to soil forming factor organism (including land cover and land use)
        -   [relief](data/covariates/relief/) - README and reclassification table files of covariates related to soil forming factor relief/topography
        -   [soil](data/covariates/soil/) - README and any other files of soil maps (***Note:*** These were not used in modelling framework since we did not want to model/map soil properties with existing soil maps and instead only used the soil forming factors)
        -   [covariates_metadata.csv](data/covariates/covariates_metadata.csv) - Summary metadata table of all covariates used
        -   [covariates_metadata.xlsx](data/covariates/covariates_metadata.xlsx) - Summary metadata table of all covariates used
    -   [other](data/other/) - Other spatial data not used as covariates (e.g. table of land use classes as designated in BIS database, shapefiles of provincial and country borders and probability sample strata and mapping mask used to assign "no data" values)
-   [out](out/) - Intermediary (e.g. changes made to input data) and final model outputs
    -   [out/data/model](out/data/model/) - Model evaluation and hypertuning outputs
    -   [out/figs/explorative](out/figs/explorative/) - Exploratory analysis and descriptive plots of modelling input soil point data
    -   [out/figs/models](out/figs/models/) - Model evaluation plots: accuracy plots and metrics (ME, MSE, RMSE, MEC, PICP) of different accuracy assessment (statistical validation) strategies, model residuals over space per depth layer, and variable importance measures
    -   [out/maps/explorative](out/maps/explorative/) - Exploratory analysis maps of BIS soil point data, AHN and AHN derivatives
    -   [out/maps/target](out/maps/target/) - PDF files of the BIS-4D prediction maps and depth transects of the target soil properties. Note: the GeoTIFF files at 25 m resolution are available here: <https://doi.org/10.4121/0c934ac6-2e95-4422-8360-d3a802766c71>

## References {#references}

[4 Dimensional Information About the Skin of the Earth (BIS-4D video supplement)](https://www.youtube.com/watch?v=ENCYUnqc-wo)

Arrouays, D., McBratney, A., Minasny, B., Hempel, J., Heuvelink, G.B.M., MacMillan, R.A., Hartemink, A., Lagacherie, P., McKenzie, N., 2015. The GlobalSoilMap project specifications, in: Proceedings of the 1st GlobalSoilMap Conference, pp. 9--12. <doi:https://doi.org/10.1201/b16500-4>.

[Developing a high-resolution 4-dimensional soil modelling and mapping platform for the Netherlands (BIS-4D): project website](https://www.wur.nl/en/project/Developing-a-high-resolution-4-dimensional-soil-modelling-and-mapping-platform-for-the-Netherlands-BIS-4D.htm)

Dokuchaev, V., 1899. Report to the Transcaucasian Statistical Committee on Land Evaluation in General and Especially for the Transcaucasia. Horizontal and Vertical Soil Zones. (In Russian.). Off. Press Civ, Affairs Commander-in-Chief Cacasus, Tiflis, Russia

Finke, P.A., J.J. de Gruijter en R. Visschers, 2001. Status 2001 Landelijke Steekproef Kaarteenhedenen toepassingen; Gestructureerde bemonstering en karakterisering Nederlandse bodems. Wageningen, Alterra, Research Instituut voor de Groene Ruimte. Alterra-rapport 389. <https://library.wur.nl/WebQuery/wurpubs/316229>

Helfenstein, A., Mulder, V. L., Hack-ten Broeke, M. J. D., van Doorn, M., Teuling, K., Walvoort, D. J. J., and Heuvelink, G. B. M.: BIS-4D: Mapping soil properties and their uncertainties at 25 m resolution in the Netherlands, Earth Syst. Sci. Data Discuss. [preprint], <https://doi.org/10.5194/essd-2024-26>, accepted, 2024a.

Helfenstein, A., Mulder, V.L., Heuvelink, G.B.M., Hack-ten Broeke, M.J.D., 2024b. Three-dimensional space and time mapping reveals soil organic matter decreases across anthropogenic landscapes in the Netherlands. Communications Earth & Environment 5, 1--16. <https://doi.org/10.1038/s43247-024-01293-y>

Helfenstein, A., Mulder, V.L., Heuvelink, G.B., Okx, J.P., 2022. Tier 4 maps of soil pH at 25 m resolution for the Netherlands. Geoderma 410, 115659. [doi:10.1016/j.geoderma.2021.115659](https://doi.org/10.1016/j.geoderma.2021.115659)

Hengl, T., MacMillan, R.A., 2019. Predictive Soil Mapping with R. OpenGeoHub foundation, Wageningen, the Netherlands. <https://soilmapper.org/>

[ISRIC's tweaked QRF predict function](https://git.wur.nl/isric/soilgrids/soilgrids/-/blob/master/models/ranger/predict_qrf_fun.R). Code for public release of [Poggio et al., 2021](https://doi.org/10.5194/soil-7-217-2021). soilgrids/models/ranger/predict_qrf_fun.R

Jenny, H., 1941. Factors of Soil Formation: A System of Quantitative Pedology. McGraw- Hill, New York.

Loiseau, T., Chen, S., Mulder, V.L., Román Dobarco, M., Richer-de-Forges, A.C., Lehmann, S., Bourennane, H., Saby, N.P.A., Martin, M.P., Vaudour, E., Gomez, C., Lagacherie, P., Arrouays, D., 2019. Satellite data integration for soil clay content modelling at a national scale. International Journal of Applied Earth Observation and Geoinformation 82, 101905. <https://doi.org/10.1016/j.jag.2019.101905>

Poggio, L., de Sousa, L.M., Batjes, N.H., Heuvelink, G.B.M., Kempen, B., Ribeiro, E., Rossiter, D., 2021. SoilGrids 2.0: Producing soil information for the globe with quantified spatial uncertainty. SOIL 7, 217--240. [doi:[https://doi.org/10.5194/soil-7-217-2021](doi:%5Bhttps://doi.org/10.5194/soil-7-217-2021){.uri}](https://doi.org/10.5194/soil-7-217-2021)

[Stackoverflow question: Split a raster into tiles in R](https://stackoverflow.com/questions/52484216/split-a-raster-into-5-pixel-x-5-pixel-tiles-in-r)

Van Tol-Leender, D., Knotters, M., de Groot, W., Gerritsen, P., Reijneveld, A., van Egmond, F., Wösten, H., & Kuikman, P. (2019). Koolstofvoorraad in de bodem van Nederland (1998-2018): CC-NL. (Wageningen Environmental Research rapport; No. 2974). Wageningen Environmental Research. <https://doi.org/10.18174/509781>

Visschers, R., Finke, P.A., de Gruijter, J.J., 2007. A soil sampling program for the Netherlands. Geoderma 139, 60--72. [doi:[https://doi.org/10.1016/j.geoderma.2007.01.008](doi:%5Bhttps://doi.org/10.1016/j.geoderma.2007.01.008){.uri}](https://doi.org/10.1016/j.geoderma.2007.01.008)

## Funding

This project was financially supported by the Dutch Ministry of Agriculture, Nature and Food Quality ([WOT-04-013-010](https://research.wur.nl/en/projects/soil-property-mapping-wot-04-013-010) and [KB-36-001-014](https://research.wur.nl/en/projects/bro-soil-properties-analyses-spa-kb-36-001-014)).
