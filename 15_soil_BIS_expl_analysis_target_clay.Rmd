---
title: "Exploratory Analysis of Clay Content for BIS-3D"
author: "Anatol Helfenstein"
date: "`r Sys.Date()`"
output:
  html_document:
    toc: yes
    toc_float: yes
  '': default
---



```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
options(width = 100) # sets width of R code output (not images)
```

```{r load required pkgs and load data, include = FALSE}
# required packages
pkgs <- c("tidyverse", "sf", "leaflet", "leafgl")
# make sure 'mapview' and/or 'leafgl' pkgs installed from github to avoid pandoc error:
# remotes::install_github("r-spatial/mapview")
# devtools::install_github("r-spatial/leafgl")

lapply(pkgs, library, character.only = TRUE)

# read in all Dutch soil point data from the BIS DB
tbl_BIS <- read_rds("out/data/soil/01_tbl_BIS.Rds")

# for now, ignore spatial support & assume midpoint of each depth increment = d_mid
tbl_BIS <- tbl_BIS %>% 
  mutate(d_mid = (d_lower - d_upper)/2 + d_upper,
         .after = d_lower)
```



***
# Soil properties in Dutch soil database (BIS)

There are a range of different soil properties in the Dutch soil database, or "Bodemkundig informatie systeem" (BIS). See figure below for a flowchart of the current BIS database (version 7.4).

```{r BIS versie, out.width="100%", fig.align = "center", fig.cap="Flowchart of current BIS database, version 7.4", echo = FALSE}
knitr::include_graphics(path = "db/2020-01-01_organigram_flowchart_BIS.jpg")
```

Possible target soil properties were nested into the list-column `soil_target` and include properties related to soil organic carbon (SOC) and soil organic matter (SOM), pH, soil texture (clay, silt, sand), nitrogen (N), phosphorus (P), cation exchange capacity (CEC) and others. Additional soil properties not considered as potential targets that are related to soil chemical properties, soil physical properties or remaining non-chemical and non-physical soil properties are in nested list-columns `soil_chem`, `soil_phys` and `soil_other`, respectively. Additional aggregated information related to the soil profile, environmental factors, metadata and other unknown variables (if available) for each soil observation are also included as nested list-columns.

```{r target variables, include = TRUE, echo = TRUE}
tbl_BIS

# get an overview of possible target soil properties (response variable):
tbl_BIS %>% 
  dplyr::select(soil_target) %>% 
  unnest_legacy() %>% 
  colnames()

```

As of now, we will limit the exploratory analysis to the top-priority target soil properties (response variables) for implementing a high-resolution soil information system for the Netherlands in 3D (BIS-3D). Here, we focus on clay content.



***
# Clay content

***
## Different clay content variables in BIS

First we will look at the different information available for clay content and see what is really useful for BIS-3D...

```{r clay different methods, include = TRUE, echo = TRUE, warning = FALSE, message = FALSE}
# extract target variables
tbl_BIS_clay <- tbl_BIS %>% 
  unnest(soil_target) %>% 
  dplyr::select(BIS_tbl:year,
                contains("clay"),
                soil_chem:unknown) %>% 
  filter_at(vars(contains("clay")), any_vars(!is.na(.)))

# different clay content variables
# "clay_NIR_min_gkg" in g/kg, remaining in %
tbl_BIS_clay %>% 
  dplyr::select(contains("clay")) %>% 
  summary()

# Predictions of clay content based on NIR in the CCNL dataset
tbl_BIS_clay %>%
  filter(!(clay_NIR_per %in% NA & clay_NIR_min_gkg %in% NA)) %>% 
  pull(BIS_tbl) %>% 
  unique()

# Remove NIR predictions
tbl_BIS_clay <- tbl_BIS_clay %>% 
  dplyr::select(-contains("_NIR_"))


# Check if there are overlapping clay measurements from different cols ---------

# here there are multiple observations of clay content
tbl_BIS_clay %>% 
    filter(!clay_est_per %in% NA & !clay_est_avg_per %in% NA)

tbl_BIS_clay %>% 
    filter(!clay_est_per %in% NA & !clay_per %in% NA)

tbl_BIS_clay %>% 
    filter(!clay_est_avg_per %in% NA & !clay_per %in% NA)

# for table with multiple observations, we compare values:
tbl_BIS_clay %>%
  filter(!clay_est_per %in% NA & !clay_est_avg_per %in% NA) %>%
  filter(clay_est_per != clay_est_avg_per)
# only 5 unequal observations and they differ by 1%, so negligible


# explore metadata of clay content cols ----------------------------------------

unnest(tbl_BIS_clay, cols = metadata)
```

Clay content variables in BIS:

* "clay_est_per": Field estimations/approximations of clay content [%] from BPK and PFB datasets
* "clay_NIR_per": NIR spectroscopy estimations of clay content [%] from CCNL dataset
* "clay_NIR_min_gkg": Mineral-corrected NIR spectroscopy estimations of clay content [g/kg] from CCNL dataset
* "clay_est_avg_per": Field estimations/approximations of clay content [%] from BPK and PFB datasets; bulk/mixed samples; averaged over several samples within a given area, e.g. several locations directly next to each other
* "clay_per": Measurements/lab analysis of clay content [g/kg] from PFB dataset


***
## Clay content [%]

So we can harmonize into one "clay [%]" target variable. We will not use NIR spectroscopy estimates in CCNL because CCNL is only used for validation and we do not want to validate models using NIR spectroscopy estimates. Hence, no clay data is available for statistical validation using a separate dataset. Furthermore, it is important that field approximations/estimations and lab measurements in the model calibration data (PFB and BPK) can still be differentiated via the "BIS_type" column.

We will now do some exploratory analysis of the BIS-3D target variable "clay content [%]". First we will look at the distribution of all observations (field estimates and lab measurements; n = approx. 690'000) ignoring 3D space.

```{r clay all, include = TRUE, echo = TRUE, out.width = "100%", warning = FALSE, message = FALSE}
(sf_clay <- tbl_BIS_clay %>% 
   # harmonize into one clay content [%] col
   mutate(clay_per = rowMeans(dplyr::select(., contains("clay")), na.rm = TRUE),
          .keep = "unused") %>% # for some reason ".keep" arg doesn't work here!
   dplyr::select(BIS_tbl:year, clay_per, soil_chem:unknown) %>%
   # remove NA values
   filter(!clay_per %in% NA) %>% 
   filter(!clay_per %in% NaN) %>% 
   # convert to sf points object but keep X and Y coordinates as col
   st_as_sf(., coords = c("X", "Y"), crs = "EPSG:28992") %>%
   mutate(X = unname(st_coordinates(.$geometry)[,1]),
          Y = unname(st_coordinates(.$geometry)[,2])) %>%
   group_by(X, Y)) # so that we can map just 1 samples per location later...
# why is this not the same as grouping by site_id? Are site_id's not unique
# among both BPK and PFB???

# plot histogram
sf_clay %>% 
  ggplot(aes(clay_per)) +
  geom_histogram(binwidth = 1) +
  scale_y_continuous() +
  scale_x_continuous(breaks = seq(0, 100, 10)) +
  xlab("clay [%]") + 
  theme_bw() +
  theme(axis.title.y = element_blank())

# only select uppermost sample for mapping locations and transform for interactive map
sf_clay_upper_trans <- sf_clay %>%
  st_transform(., crs = "EPSG:4326") %>% 
  slice(1L)

# colors
colors_clay <- colourvalues::colour_values_rgb(sf_clay_upper_trans$clay_per,
                                               palette = "magma",
                                               include_alpha = FALSE)

# Interactive map of uppermost clay value at each location
leaflet() %>%
  setView(lng = 5.3, lat = 52.1, zoom = 7) %>% 
  addProviderTiles(provider = providers$OpenStreetMap) %>%
  addGlPoints(data = sf_clay_upper_trans,
              fillColor = colors_clay,
              group = "sf_clay_upper_trans",
              label = as.character(sf_clay_upper_trans$clay_per))
# how to give map title and legend?
```


***
## Clay content [%]: lab measurements

The majority of the clay [%] observations are field estimates. Now we look at the distribution of only the lab measurements (n = 15'786)...

```{r clay lab, include = TRUE, echo = TRUE, out.width = "100%", warning = FALSE, message = FALSE}
(sf_clay_lab <- filter(sf_clay, BIS_type %in% "lab"))

# Clay was only measured using conventional laboratory methods in the PFB dataset
unique(sf_clay_lab$BIS_tbl)

# plot histogram
sf_clay_lab %>% 
  ggplot(aes(clay_per)) +
  geom_histogram(binwidth = 1) +
  scale_y_continuous() +
  scale_x_continuous(breaks = seq(0, 100, 10)) +
  xlab("clay [%]") + 
  theme_bw() +
  theme(axis.title.y = element_blank())

# only select uppermost sample for mapping locations and transform for interactive map
sf_clay_lab_upper_trans <- sf_clay_lab %>%
  st_transform(., crs = "EPSG:4326") %>% 
  slice(1L)

# colors
colors_clay_lab <- colourvalues::colour_values_rgb(sf_clay_lab_upper_trans$clay_per,
                                                   palette = "magma",
                                                   include_alpha = FALSE)

# Interactive map of uppermost clay value at each location
leaflet() %>%
  setView(lng = 5.3, lat = 52.1, zoom = 7) %>% 
  addProviderTiles(provider = providers$OpenStreetMap) %>%
  addGlPoints(data = sf_clay_lab_upper_trans,
              fillColor = colors_clay_lab,
              group = "sf_clay_lab_upper_trans",
              label = as.character(sf_clay_lab_upper_trans$clay_per))
```

The PFB is a collection of soil samples that were taken by horizon between 1953 - 2012. Below is the exploratory analysis of the lab measurements from PFB:

```{r clay PFB lab, include = TRUE, echo = TRUE, out.width = "100%", warning = FALSE, message = FALSE}
(sf_clay_pfb <- filter(sf_clay_lab, BIS_tbl %in% "PFB"))

# plot histogram
sf_clay_pfb %>% 
  ggplot(aes(clay_per)) +
  geom_histogram(binwidth = 1) +
  scale_y_continuous() +
  scale_x_continuous(breaks = seq(0, 100, 10)) +
  xlab("clay [%]") + 
  theme_bw() +
  theme(axis.title.y = element_blank())

# only select uppermost sample for mapping locations and transform for interactive map
sf_clay_pfb_upper_trans <- sf_clay_pfb %>%
  st_transform(., crs = "EPSG:4326") %>% 
  slice(1L)

# colors
colors_clay_pfb <- colourvalues::colour_values_rgb(sf_clay_pfb_upper_trans$clay_per,
                                                    palette = "magma",
                                                    include_alpha = FALSE)

# Interactive map of uppermost clay value at each location
leaflet() %>%
  setView(lng = 5.3, lat = 52.1, zoom = 7) %>% 
  addProviderTiles(provider = providers$OpenStreetMap) %>%
  addGlPoints(data = sf_clay_pfb_upper_trans,
              fillColor = colors_clay_pfb,
              group = "sf_clay_pfb_upper_trans",
              label = as.character(sf_clay_pfb_upper_trans$clay_per))

# might also want to plot year of sampling
# colors
colors_clay_pfb_year <- colourvalues::colour_values_rgb(sf_clay_pfb_upper_trans$year,
                                                    palette = "viridis",
                                                    include_alpha = FALSE)

# Interactive map of sampling year at each location
leaflet() %>%
  setView(lng = 5.3, lat = 52.1, zoom = 7) %>% 
  addProviderTiles(provider = providers$OpenStreetMap) %>%
  addGlPoints(data = sf_clay_pfb_upper_trans,
              fillColor = colors_clay_pfb_year,
              group = "sf_clay_pfb_upper_trans",
              label = as.character(sf_clay_pfb_upper_trans$year))
```


